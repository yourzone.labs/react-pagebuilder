import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import reportWebVitals from './reportWebVitals';
import { MoralisProvider } from "react-moralis";

//const APP_ID = process.env.REACT_APP_MORALIS_APPLICATION_ID;  // not working
//const SERVER_URL = process.env.REACT_APP_MORALIS_SERVER_URL;  // not working

ReactDOM.render(
  <React.StrictMode>
    <MoralisProvider appId={'CfK9EyEy66AnKqB0pE3QNGlJVfmkZKWcTuN96IUi'} serverUrl={'https://unevdfgumk4k.usemoralis.com:2053/server'}>
      <App />
    </MoralisProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.unregister();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
