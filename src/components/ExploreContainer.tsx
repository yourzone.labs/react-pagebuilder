import { useMoralis } from "react-moralis";

import './ExploreContainer.css';

interface ContainerProps {
  name: string;
}

const ExploreContainer: React.FC<ContainerProps> = ({ name }) => {
  const { isAuthenticated } = useMoralis();

  return (
    <div className="container">
      <strong>{name}</strong>
      {isAuthenticated && <span>authenticated</span>}
      <p>Explore <a target="_blank" rel="noopener noreferrer" href="https://ionicframework.com/docs/components">UI Components</a></p>
    </div>
  );
};

export default ExploreContainer;
