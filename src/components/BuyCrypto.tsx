import { IonButton, IonLabel, IonItem, IonModal, IonContent } from '@ionic/react';
import React, { useState } from 'react';
import { useMoralis } from "react-moralis";

const BuyCrypto: React.FC = () => {
	const { Moralis } = useMoralis();
	const [ showModal, setShowModal ] = useState(false);
	const [ iframeSrc, setIframeSrc ] = useState();

	async function buyCrypto() {
		let result = await Moralis.Plugins.fiat.buy({}, {disableTriggers: true});
		setIframeSrc(result.data);
		setShowModal(true);
	}

	return (
		<IonItem>
			<IonButton onClick={buyCrypto}>Buy Crypto</IonButton>
			<IonModal isOpen={showModal}>
				<iframe src={iframeSrc} allowFullScreen={true} frameBorder="0" width="100%" height="90%"></iframe>
	        	<IonButton onClick={() => setShowModal(false)}>Back</IonButton>
	      	</IonModal>
		</IonItem>
  	);
};

export default BuyCrypto;
