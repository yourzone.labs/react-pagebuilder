import { IonButton, IonLabel, IonItem } from '@ionic/react';
import React from 'react';
import { useMoralis } from "react-moralis";

const ConnectWallet: React.FC = () => {
  const { authenticate, isWeb3Enabled, enableWeb3, Moralis, isAuthenticated, isAuthenticating, user, account, logout } = useMoralis();
  
  async function connectWallet() {
    await authenticate({signingMessage:'Hello!'});
    console.log("connected");
  }

  async function disconnectWallet() {
    await logout();
    console.log("disconnected");
  }

  if (!isAuthenticated) {
    return (
      <IonButton onClick={connectWallet}>Connect Wallet</IonButton>
    );
  }

  const currentUser = Moralis.User.current();
  
  return (
      <IonItem>
        {currentUser && <IonLabel>{currentUser.get('ethAddress')}</IonLabel>}
        <IonButton onClick={disconnectWallet}>Disconnect</IonButton>
      </IonItem>
  );
};

export default ConnectWallet;

